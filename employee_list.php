<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 11/8/15
 * Time: 9:39 PM
 */
error_reporting('no');
session_start();

include('navbar.php');
include('pdo.php');
include('database.php');

$dbconnection = $connection_object->connection('localhost', $dbusername, $dbpassword, $dbname);



$view = "SELECT * FROM `add_employee` ";
$data = $dbconnection->query($view);
//echo $data;
print_r($data);
//if(isset($_POST['submit'])){
//
//}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        .select-form{
            margin-bottom: 15px;
            margin-left: 100px;
        }
    </style>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-2">
        <form action="" method="POST" role="form" class="form-inline select-form">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
<!--                        <div class="form-group">-->
<!--                            <input type="text" class="form-control" placeholder="Search">-->
<!--                        </div>-->
<!--                        <button type="submit" class="btn btn-default">Submit</button>-->
                        <select class="form-control" id="select">
                            <option value="">IT</option>
                            <option value="">Student</option>
                            <option>Mentor</option>
                            <option>Manager</option>
                            <option>HR</option>
                            <option>Clark</option>
                        </select>
                        <button type="submit" name="submit" class="btn btn-default">Search</button>
                    </div>
                </div>
            </div>


        </form>
    </div>

<!--    ============= TABLE =============-->
    <div class="col-md-7 col-md-offset-2">
        <table class="table table-bordered table-hover">
        	<thead class="text-center">
        		<tr>
        			<th class="text-center">SL</th>
                    <th class="text-center">Name</th>
        			<th class="text-center">E-Id</th>
        			<th class="text-center">Age</th>
        			<th class="text-center">Department</th>
        			<th class="text-center">Designation</th>
        			<th class="text-center">Salary</th>
        			<th class="text-center">Email</th>
        			<th class="text-center">Password</th>
        			<th class="text-center">Action</th>
        		</tr>
        	</thead>
        	<tbody>
        		<tr class="text-center">
        			<td>1</td>
        			<td>Name</td>
        			<td>#543</td>
        			<td>IT</td>
        			<td>1</td>
        			<td>1</td>
        			<td>1</td>
        			<td>1</td>
        			<td>Admin</td>
        			<td><a href="#">View</a></td>
        		</tr>

        	</tbody>
        </table>
    </div>
</div>
</body>
</html>

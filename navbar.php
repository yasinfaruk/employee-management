<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 11/7/15
 * Time: 12:42 PM
 */
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        #navigation{
            background: white;
            color: white;
        }
        #navigation > ul{
            margin-left: 30%;
        }
        #navigation > ul > li{
            border-left: 1px solid #eeeeee;
            display: block;
            
        }
        #navigation > ul > li:first-child{
            border-left: none;
        }
        #navigation > ul > li > a{
            color: grey;
            transition: 0.3s;
            -webkit-transition: 0.3s;
            font-size: 15px;
        }
        #navigation > ul > li:hover a{
            color: coral;
            /*font-size: 18px;*/
            background: whitesmoke;
            /*display: block;*/
            border-bottom: 3px solid orangered;
            overflow: hidden;
            outline: 0;
        }
    </style>
</head>
<body>
    <div class="row">
        <nav class="navbar navbar-default" role="navigation" id="navigation">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        		</button>
        	</div>
        
        	<!-- Collect the nav links, forms, and other content for toggling -->
        	<div class="collapse navbar-collapse navbar-ex1-collapse" id="navigation">
        		<ul class="nav navbar-nav">
        			<li><a href="#">Home</a></li>
                                <li><a href="add_employee.php">Add Employee</a></li>
        			<!--<li><a href="#">Employee List</a></li>-->
        			<!--<li><a href="#">Accounts</a></li>-->
                                <li><a href="logout.php">Logout</a></li>
        		</ul>

        	</div><!-- /.navbar-collapse -->
        </nav>
    </div>
</body>
</html>
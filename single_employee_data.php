<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 11/8/15
 * Time: 9:39 PM
 */
error_reporting('no');
include('navbar.php');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <style>
        .employee-image {
            border: 8px solid whitesmoke;
            border-radius: 5px;
            box-shadow: 0px 0px 5px 1px grey;
            /*margin-bottom: 30px;*/
            width: 150px;
            height: 150px;
        }
        .employee_part{
            margin-bottom: 30px;
        }
        .employee_info {
            /*border: 1px solid grey;*/
            font-size: 18px;
            padding: 15px 0px 0px 80px;
        }
        .week-form{
            margin-bottom: 20px;
            margin-top: 0px;
        }
        .info-table {
            margin-left: 45px;
        }
    </style>
</head>
<body>
<div class="row">
<!--    ============== view weeek =================-->
    <div class="col-md-12 text-center">
        <form action="" class="form-inline week-form" method="POST" role="form">
        
        	<div class="form-group">
        		<label for=""></label>
        		<input type="text" class="form-control datepicker" name="" id="" placeholder="From">
        		<input type="text" class="form-control datepicker" name="" id="" placeholder="To">
        	</div>

        	<button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
<!--============== end week ===============-->
    <div class="col-md-8 col-md-offset-2 employee_part">
        <div class="col-md-6">
            <div class="employee_info text-muted">
                <span><em>Name : </em></span><br>
                <span><em>E-ID : </em></span><br>
                <span><em>Department : </em></span><br>
                <span><em>Designation : </em></span><br>
                <span><em>Date : </em></span>
            </div>
        </div>
        <div class="col-md-6">
            <img src="https://goo.gl/KDsthX" class="employee-image pull-right" alt="yasinfaruk">
        </div>

    </div>

        <!--    ============= TABLE =============-->
        <div class="col-md-11 info-table">
            <div class="col-md-12">
                <table class="table table-bordered table-hover">
                    <thead class="text-center">
                    <tr>
                        <th class="text-center">SL</th>
<!--                        <th class="text-center">Name</th>-->
<!--                        <th class="text-center">E-Id</th>-->
                        <th class="text-center">Date</th>
<!--                        <th class="text-center">Dept.</th>-->
<!--                        <th class="text-center">Status</th>-->
                        <th class="text-center">Time-In</th>
                        <th class="text-center">Time-Out</th>
                        <th class="text-center">Explain</th>
                        <th class="text-center">Present</th>
                        <th class="text-center">Absence</th>
                        <th class="text-center">Leave</th>
                        <th class="text-center">Vacation</th>
<!--                        <th class="text-center">V-Used</th>-->
                        <th class="text-center">Overtime</th>
<!--                        <th class="text-center">Sick</th>-->
                        <th class="text-center">T-Present</th>
                        <th class="text-center">T-Absent</th>
                        <th class="text-center">T-Leave</th>
                        <th class="text-center">T-Hour</th>
                        <th class="text-center">Salary</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="text-center">
                        <td>1</td>
                        <td>9.00</td>
                        <td>5.00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td><a href="">view</a></td>
                    </tr>
                    <tr class="text-center">
                        <td>1</td>
                        <td>9.00</td>
                        <td>5.00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td><a href="">view</a></td>
                    </tr>
                    <tr class="text-center">
                        <td>1</td>
                        <td>9.00</td>
                        <td>5.00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>00</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td><a href="">view</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!--=================== jquery ==================-->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $( ".datepicker" ).datepicker();
        });
    </script>
</body>
</html>
